package tdd.training.bsk;

public class Frame {
	
	protected int firstThrow;
	protected int secondThrow;
	protected int bonus;
	
	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
		this.bonus = 0;
		
		if((firstThrow + secondThrow > 10) || (firstThrow < 0) || (secondThrow < 0))
			throw new BowlingException("Invalid throw");
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		return this.secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return this.bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 * @throws BowlingException 
	 */
	public int getScore() throws BowlingException {
		int score = 0;
		score = this.getFirstThrow() + this.getSecondThrow() + this.getBonus();
		return score;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		boolean flag = false;
		if((this.firstThrow == 10) && (this.firstThrow + this.secondThrow == 10))
			flag = true;
		return flag;
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		boolean flag = false;
		if((this.firstThrow + this.secondThrow == 10) && (this.firstThrow != 10))
				flag = true;
		return flag;
	}

}
