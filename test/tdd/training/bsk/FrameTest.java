package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	// User story 1
	public void testValidFrame() throws BowlingException{
		int firstThrow = 1;
		int secondThrow = 4;
		try {
			Frame frame = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow, frame.getFirstThrow());
			assertEquals(secondThrow, frame.getSecondThrow());
		} catch(BowlingException b) {
		System.err.println(b.getMessage());
		}
	}
	
	@Test
	// User story 1
	public void testInvalidFrame() throws BowlingException{
		int firstThrow = 11;
		int secondThrow = 2;
		try {
			Frame frame = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow, frame.getFirstThrow());
			assertEquals(secondThrow, frame.getSecondThrow());
		} catch(BowlingException b) {
		System.err.println(b.getMessage());
		}
	}
	
	@Test
	// User story 1
	public void testNegativeThrow() throws BowlingException{
		int firstThrow = 7;
		int secondThrow = -2;
		try {
			Frame frame = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow, frame.getFirstThrow());
			assertEquals(secondThrow, frame.getSecondThrow());
		} catch(BowlingException b) {
		System.err.println(b.getMessage());
		}
	}
	
	@Test
	// User story 2
	public void testFrameScore() throws BowlingException{
		int score = 10;
		Frame frame = new Frame(5, 5);
		try {
			assertEquals(score, frame.getScore());
		} catch(BowlingException b) {
		System.err.println(b.getMessage());
		}
	}
	
	@Test
	// User story 5
	public void testIsSpare() throws BowlingException{
		Frame frame = new Frame(4, 6);
		assertTrue(frame.isSpare());
	}
	
	@Test
	// User story 5
	public void testFrameScoreWithSpare() throws BowlingException{
		int score = 10;
		int bonus = 5;
		Frame frame = new Frame(6, 4);
		frame.setBonus(bonus);
		assertEquals(score+bonus, frame.getScore());	
	}
	
	@Test
	// User story 6
	public void testIsStrike() throws BowlingException{
		Frame frame = new Frame(10, 0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	// User story 6
	public void testFrameScoreWithStrike() throws BowlingException{
		int score = 10;
		int bonus = 8;
		Frame frame = new Frame(10, 0);
		frame.setBonus(bonus);
		assertEquals(score+bonus, frame.getScore());	
	}
}
